
<a href="<?= url('node/' . $node->nid) ?>" title="<?= $long_title ?>">

	<div class="image"><?= $image('section_big') ?></div>

	<div class="genre"><?= $genre ?></div>

	<h3 class="title"><?= $title ?></h3>

	<div class="teaser"><?= $body(40) ?></div>

</a>
