
<a href="<?= url('node/' . $node->nid) ?>" title="<?= $long_title ?>">

	<div class="image"><?= $image('section_small') ?></div>

	<h3 class="title"><?= $title ?></h3>

	<div class="date-author"><?= $date ?> | <?= $source ?></div>

	<div class="teaser"><?= $body(40) ?></div>

</a>
