
<a href="<?= url('node/' . $node->nid) ?>" title="<?= $long_title ?>">

	<div class="genre"><?= $genre ?></div>

	<div class="image"><?= $image('section_small') ?></div>

	<h3 class="title"><?= $title ?></h3>

	<div class="section"><?= $section ?></div>

</a>
